using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TrashManager : MonoBehaviour
{
    public GameObject prefab, prefabcam, prefabkag�t;
    public GameObject panel;
    int counter;
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Trash Plastik")
        {
            panel.SetActive(true);
            panel.GetComponentInChildren<Text>().text = "E ile Plasti�i ��pe At";
            if (Input.GetKeyDown(KeyCode.E))
            {
                counter++;
                if (FindObjectOfType<Collector>().sayacPlastik < 1)
                {
                    Debug.Log("cop kalmad�");
                }
                else
                {
                    GameObject temp = Instantiate(prefab, GameObject.Find("SpawnPointP").transform.position, Quaternion.identity);
                    temp.GetComponent<Rigidbody>().useGravity = true;
                    temp.GetComponent<BoxCollider>().isTrigger = false;
                    temp.GetComponent<BoxCollider>().size = new Vector3(1, 1, 1);
                    FindObjectOfType<Collector>().sayacPlastik--;
                    FindObjectOfType<Collector>().textp.text = FindObjectOfType<Collector>().sayacPlastik.ToString();
                    GameObject.FindObjectOfType<ListManager>().b�rakma.Add(temp);
                    GameObject.FindObjectOfType<ListManager>().CheckEmpty();
                }
            }
        }
        else if (other.gameObject.tag == "Trash Cam ")
        {
            panel.SetActive(true);
            panel.GetComponentInChildren<Text>().text = "E ile Cam� ��pe At";
            if (Input.GetKeyDown(KeyCode.E))
            {
                counter++;
                if (FindObjectOfType<Collector>().sayacCam < 1)
                {
                    Debug.Log("cop kalmad�");
                }
                else
                {
                    GameObject temp = Instantiate(prefabcam, GameObject.Find("SpawnPointC").transform.position, Quaternion.identity);
                    temp.GetComponent<Rigidbody>().useGravity = true;
                    temp.GetComponent<BoxCollider>().isTrigger = false;
                    temp.GetComponent<BoxCollider>().size = new Vector3(1, 1, 1);
                    FindObjectOfType<Collector>().sayacCam--;
                    FindObjectOfType<Collector>().textc.text = FindObjectOfType<Collector>().sayacCam.ToString();
                    GameObject.FindObjectOfType<ListManager>().b�rakma.Add(temp);
                    GameObject.FindObjectOfType<ListManager>().CheckEmpty();
                }
            }
        }
        else if (other.gameObject.tag == "Trash Kag�t")
        {
            panel.SetActive(true);
            panel.GetComponentInChildren<Text>().text = "E ile Ka��d� ��pe At";
            if (Input.GetKeyDown(KeyCode.E))
            {
                counter++;
                if (FindObjectOfType<Collector>().sayacKagit < 1)
                {
                    Debug.Log("cop kalmad�");
                }
                else
                {
                    GameObject temp = Instantiate(prefabkag�t, GameObject.Find("SpawnPointK").transform.position, Quaternion.identity);
                    temp.GetComponent<Rigidbody>().useGravity = true;
                    temp.GetComponent<BoxCollider>().isTrigger = false;
                    temp.GetComponent<BoxCollider>().size = new Vector3(1, 1, 1);
                    FindObjectOfType<Collector>().sayacKagit--;
                    FindObjectOfType<Collector>().textk.text = FindObjectOfType<Collector>().sayacKagit.ToString();
                    GameObject.FindObjectOfType<ListManager>().b�rakma.Add(temp);
                    GameObject.FindObjectOfType<ListManager>().CheckEmpty();
                }
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        panel.SetActive(false);
    }
}
