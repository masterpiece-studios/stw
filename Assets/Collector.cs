using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Collector : MonoBehaviour
{
    public GameObject panel;
    public int count;
    public int sayacPlastik, sayacCam, sayacKagit;
    public Text textp, textc, textk;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Plastik")
        {
            panel.SetActive(true);
            panel.GetComponentInChildren<Text>().text = "E ile Plasti�i Topla";
        }
        else if (other.gameObject.tag=="Cam")
        {
            panel.SetActive(true);
            panel.GetComponentInChildren<Text>().text = "E ile Cam� Topla";
        }
        else if (other.gameObject.tag == "Kag�t")
        {
            panel.SetActive(true);
            panel.GetComponentInChildren<Text>().text = "E ile Ka��d� Topla";
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag=="Plastik")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                sayacPlastik++;
                textp.text = sayacPlastik.ToString();
               GameObject.FindObjectOfType<ListManager>().toplama.Add(other.gameObject);
                Destroy(other.gameObject);
                count++;
                panel.SetActive(false);
            }
        }
        else if (other.gameObject.tag == "Cam")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                sayacCam++;
                textc.text = sayacCam.ToString();
                GameObject.FindObjectOfType<ListManager>().toplama.Add(other.gameObject);
                Destroy(other.gameObject);
                count++;
                panel.SetActive(false);
            }
        }
        else if (other.gameObject.tag=="Kag�t")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                sayacKagit++;
                textk.text = sayacKagit.ToString();
                GameObject.FindObjectOfType<ListManager>().toplama.Add(other.gameObject);
                Destroy(other.gameObject);
                count++;
                panel.SetActive(false);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        panel.SetActive(false);
    }
    }
